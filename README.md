# Frontend Mentor - Profile card component solution

This is a solution to the [Profile card component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/profile-card-component-cfArpWshJ). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

## Overview

### The challenge

- Build out the project to the designs provided

### Screenshot

![Mobile](./screenshots/mobile.jpg)
![Desktop](./screenshots/desktop.jpg)

### Links

- Solution URL: [Gitlab Repository](https://gitlab.com/frontend-mentor-challenge/14-profile-card-component)
- Live Site URL: [Live](https://profile-card-component-t0ol.onrender.com)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow
- [React](https://reactjs.org/) - JS library
- [Normalize](https://necolas.github.io/normalize.css/) - CSS Reset
- [Google Fonts](https://fonts.google.com/)
- [SASS](https://sass-lang.com/) - SASS

### What I learned

### Continued development

### Useful resources

- [Figma](https://figma.com) - For design
- [React](https://es.react.dev/reference/react) - For resolving doubts

## Author

- Website - [Issac Leyva](In construction)
- Frontend Mentor - [@issleyva](https://www.frontendmentor.io/profile/issleyva)

## Acknowledgments