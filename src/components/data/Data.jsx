import './Data.scss'

export default function DataProfile() {
    return (
        <div className="card__info">
            <h1 className="card__name">Victor Crest<span>26</span></h1>
            <p className="card__city">London</p>
        </div>
    )
}