import './Header.scss'
import victorImage from '../../assets/images/image-victor.jpg'

export default function Header() {
    return (
        <header className="card__header">
            <img src={victorImage} alt="" />
        </header>
    )
}