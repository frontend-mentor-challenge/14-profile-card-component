import './Footer.scss'

export default function Footer() {
    return (
        <footer className="card__data">
            <p className='card__data-totals'>80K <span>Followers</span></p>
            <p className='card__data-totals'>803K <span>Likes</span></p>
            <p className='card__data-totals'>1.4K <span>Photos</span></p>
        </footer>
    )
}