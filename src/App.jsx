import Header from './components/header/Header'
import DataProfile from './components/data/Data'
import Footer from './components/footer/Footer'
import './App.scss'

function App() {

  return (
    <>
    <main className="card">
      <Header />
      <DataProfile />
      <Footer />
    </main>
    </>
  )
}

export default App
